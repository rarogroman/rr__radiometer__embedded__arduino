#include <stdio.h>

#define ENA_STEPPER_HORIZONTAL PD0                // Microcontroller ports to which the stepper driver is connected
#define DIR_STEPPER_HORIZONTAL PD1
#define STP_STEPPER_HORIZONTAL PD2

#define ENA_STEPPER_VERTICAL PD3                  // Microcontroller ports to which the stepper driver is connected
#define DIR_STEPPER_VERTICAL PD4
#define STP_STEPPER_VERTICAL PD5

#define BUT_LEFT PE10                             // Microcontroller ports to which the buttons left/right/up/down is connected
#define BUT_RIGHT PE9
#define BUT_UP PE7
#define BUT_DOWN PE8

/* Steppers configuration */
int stepper_delay_horizontal = 500;               // Stepper horizontal delay with control from PC
int stepper_delay_vertical = 500;                 // Stepper vertical delay with control from PC
int stepper_manual_delay_horizontaL = 50;         // Stepper horizontal delay with control from front panel buttons
int stepper_manual_delay_vertical = 500;          // Stepper vertical delay with control from front panel buttons
const int STEPPER_MANUAL_TICK_COUNT = 16;         // Number of microsteps per one stepper step

const int BUTTON_PRESS_DELAY = 200;               // Delay when pressing the button for softer controls

int command;                                      // Code command from the PC, see all available commands in README.md
char adc_value[10];                               // Analog value read from ADC

void setup() {
  serial_init();
  buttons_init();
  stepper_horizontal_init();
  stepper_vertical_init();
  adc_init();
}

void loop() {
  // Handle the front panel buttons
  if (digitalRead(BUT_LEFT) == 0) {
    delay(BUTTON_PRESS_DELAY);
    go_left();
  } else if (digitalRead(BUT_RIGHT) == 0) {
    delay(BUTTON_PRESS_DELAY);    
    go_right();
  } else if (digitalRead(BUT_UP) == 0) {
    delay(BUTTON_PRESS_DELAY);    
    go_up();
  } else if (digitalRead(BUT_DOWN) == 0) {
    delay(BUTTON_PRESS_DELAY);    
    go_down();
  }

  // Handle messages from the PC
  if (Serial1.available() > 0) {
    command = Serial1.parseInt();
    handle_command(command);
  }
}

/*
   Serial port initialization
   RX  PA10
   TX  PA9
*/
void serial_init() {
  Serial1.begin(115200);
  while ( !Serial1 ); delay(10);
  Serial1.println("Serial port initialized!");
}

/*
   Horizontal stepper initialization
*/
void stepper_horizontal_init() {
  pinMode(ENA_STEPPER_HORIZONTAL, OUTPUT);
  pinMode(DIR_STEPPER_HORIZONTAL, OUTPUT);
  pinMode(STP_STEPPER_HORIZONTAL, OUTPUT);

  digitalWrite(ENA_STEPPER_HORIZONTAL, HIGH);
  digitalWrite(DIR_STEPPER_HORIZONTAL, HIGH);
  digitalWrite(STP_STEPPER_HORIZONTAL, HIGH);
}

/*
   Vertical stepper initialization
*/
void stepper_vertical_init() {
  pinMode(ENA_STEPPER_VERTICAL, OUTPUT);
  pinMode(DIR_STEPPER_VERTICAL, OUTPUT);
  pinMode(STP_STEPPER_VERTICAL, OUTPUT);

  digitalWrite(ENA_STEPPER_VERTICAL, HIGH);
  digitalWrite(DIR_STEPPER_VERTICAL, HIGH);
  digitalWrite(STP_STEPPER_VERTICAL, HIGH);
}

void adc_init() {
  pinMode(PB0, INPUT_ANALOG);
}

/*
   Front panel buttons initialization
*/
void buttons_init() {
  pinMode(BUT_LEFT, INPUT_PULLUP);
  pinMode(BUT_RIGHT, INPUT_PULLUP);
  pinMode(BUT_UP, INPUT_PULLUP);
  pinMode(BUT_DOWN, INPUT_PULLUP);
}

/*
   Move to the left when a button on the panel is pressed
*/
void go_left() {
  digitalWrite(DIR_STEPPER_HORIZONTAL, HIGH);

  while (digitalRead(BUT_LEFT) == 0) {
    do_step(STP_STEPPER_HORIZONTAL, stepper_manual_delay_horizontaL);
  }
}

/*
   One step left
*/
void step_left() {
  digitalWrite(DIR_STEPPER_HORIZONTAL, HIGH);
  do_step(STP_STEPPER_HORIZONTAL, stepper_delay_horizontal);
}

/*
   Move to the right when a button on the panel is pressed
*/
void go_right() {
  digitalWrite(DIR_STEPPER_HORIZONTAL, LOW);
  
  while (digitalRead(BUT_RIGHT) == 0) {
    do_step(STP_STEPPER_HORIZONTAL, stepper_manual_delay_horizontaL);
  }
}

/*
   One step right
*/
void step_right() {
  digitalWrite(DIR_STEPPER_HORIZONTAL, LOW);
  do_step(STP_STEPPER_HORIZONTAL, stepper_delay_horizontal);
}

/*
   Move to the up when a button on the panel is pressed
*/
void go_up() {
  digitalWrite(DIR_STEPPER_VERTICAL, LOW);
  
  while (digitalRead(BUT_UP) == 0) {
    do_step(STP_STEPPER_VERTICAL, stepper_manual_delay_vertical);
  }
}

/*
   One step up
*/
void step_up() {
  digitalWrite(DIR_STEPPER_VERTICAL, LOW);
  do_step(STP_STEPPER_VERTICAL, stepper_delay_vertical);
}

/*
   Move to the down when a button on the panel is pressed
*/
void go_down() {
  digitalWrite(DIR_STEPPER_VERTICAL, HIGH);
  
  while (digitalRead(BUT_DOWN) == 0) {
    do_step(STP_STEPPER_VERTICAL, stepper_manual_delay_vertical);
  }
}

/*
   One step down
*/
void step_down() {
  digitalWrite(DIR_STEPPER_VERTICAL, HIGH);
  do_step(STP_STEPPER_VERTICAL, stepper_delay_vertical);
}

/*
   Handle command from the PC
*/
void handle_command(int command) {
  if (command == 0 ) return;

  switch (command) {
    case 11:
      step_left();
      break;

    case 12:
      step_right();
      break;

    case 21:
      step_up();
      break;            

    case 22:
      step_down();
      break;

    case 30:
      // Health check
      Serial1.println("OK");
      break;

    case 40:
      // Unblock horizontal stepper
      digitalWrite(ENA_STEPPER_HORIZONTAL, LOW);
      break;

    case 41:
      // Block horizontal stepper
      digitalWrite(ENA_STEPPER_HORIZONTAL, HIGH);
      break;

    case 50:
      // Unblock vertical stepper
      digitalWrite(ENA_STEPPER_VERTICAL, LOW);
      break;

    case 51:
      // Block vertical stepper
      digitalWrite(ENA_STEPPER_VERTICAL, HIGH);
      break;

    case 60:
    case 61:
    case 62:
    case 63:
    case 64:
    case 65:
    case 66:
    case 67:
    case 68:
    case 69:
      // Horizontal stepper speed
      stepper_delay_horizontal = 50 + (command - 60)*100;

    case 70:
    case 71:
    case 72:
    case 73:
    case 74:
    case 75:
    case 76:
    case 77:
    case 78:
    case 79:
      // Vertical stepper speed
      stepper_delay_vertical = 50 + (command - 70)*100;

    case 80:
    case 81:
    case 82:
    case 83:
    case 84:
    case 85:
    case 86:
    case 87:
    case 88:
    case 89:
      // Horizontal stepper manual speed - when front panel button is pressed
      stepper_manual_delay_horizontaL = 50 + (command - 80)*100;

    case 90:
    case 91:
    case 92:
    case 93:
    case 94:
    case 95:
    case 96:
    case 97:
    case 98:
    case 99:
      // Vertical stepper manual speed - when front panel button is pressed
      stepper_manual_delay_vertical = 50 + (command - 90)*100;
      
    default:
      Serial1.println("Unknown command! See the list of available commands in README.md.");
  }
  command = 0;
}

/*
 * Read data from the internal ADC
 */
void adc_internal_measure() {
  sprintf(adc_value, "ADC: %04d", analogRead(PB0));
  Serial1.println(adc_value);
  memset(adc_value, 0, sizeof adc_value);
}

/*
 * Stepper - one step
 */
void do_step(int stepper, int stepper_delay) {
  for (int i = 0; i <= STEPPER_MANUAL_TICK_COUNT; i++) {
    digitalWrite(stepper, LOW);
    delayMicroseconds(stepper_delay);
    digitalWrite(stepper, HIGH);
    delayMicroseconds(stepper_delay);
  }
  adc_internal_measure();
}
